MAILX
=====

Mailx is a small mail client optimized to handle large mbox files.
It should not be confused with the original mailx program; to prevent
that you can call this program neatmailx (just like my other neat*
programs).  After trying many different programs, I wrote mailx
together with my pop3 and smtp clients, all of which are available at
http://litcave.berlios.de/, and now I'm quite satisfied.

Mailx may not be suitable for everyone.  But it does address most of
my needs:

* can handle large mboxes fast (at the cost of memory)
* uses external pagers (see mailpg at my litcave page) and editors
* can monitor mailboxes for new mails based on file timestamps
* gives the power to edit any header field of mails before sending
* is simple and easy to change

But it lacks many features like customizable sorting, regular
expression searches, and handling mime attachments (it can decode mime
messages, though).  It also assumes mboxes are not edited concurrently
unless for appending messages.

SETTING UP
==========

mailx is configured solely by editing config.h.

* PAGER: used in page command for showing messages
* EDITOR: editor (~e command when sending mails)
* VISUAL: visual editor (~v command when sending mails)
* SENDMAIL: the command used for sending mails
* FOLDER: folder containing mboxes
* RECORD: the mbox to append sent mails
* FROM: the address to be used for sending mails
* DRAFT: address of mail draft
* HOSTNAME: hostname used for creating message ID header field
* hdr_filt[]: list of headers to include before piping to PAGER
* boxes[]: the list of incoming mboxes

* NHEAD: the number of messages to show in head command
* WIDTH: screen width
* DIGITS: the number of digits in head command
* THREADED: show messages in thread
* TRIM_RE: strip "Re:" for replied messages in head command
* COLORS: if nonzero, use colors in head command
* MAXMIME: the maximum number of decoded mime messages in memory

STARTING
========

Mailx has two modes.  In mbox browsing mode, mails in an mbox can be
read or replied to.  To start in this mode, you can give mailbox name
after -f argument (see mailboxes section for specifying mbox names):

  mailx -f +inbox

In sending mode, new mails can be composed and sent; mailx exits as soon
as the mail is sent or discarded (see writing mails section):

  mailx address

Note that using 'mail' command, mails can be composed even in mbox
browsing mode.

COMMANDS
========

Most mailx commands follow this pattern:

  [messages]cmd [arguments]

Arguments are command specific.  Messages specify which messages the
command should be applied on:

* '3': third message
* '$': the last message
* '.': current message
* ',': the next new message
* '-': the previous message
* '+': the next message
* '-3': the third previous message
* '/(subject Hello)': all messages which have "Hello" in their subject
* '/(hdr word)': all messages which have "word" in their "hdr" header
* '/n(hdr word)': like "/(hdr word)" but include only new messages
  (same could be used 'u' or ' ')
* '?(subject Hello)': search in the reverse direction
* 'X,Y': all messages between X and Y (X and Y can be any single message
  specification
* '%': all messages in current mbox

By default, all messages work on the current message which is marked with
'>' in the head command output.

Mailx commands:
* [msg]p/page: pipe current message to PAGER
* [msg]n/next/[ret]: pipe next message to PAGER
* [msg]h/ls/head/header: show message list; set the first specified
  message as current (marked with '>' in the first column)
* cd/file [mbox]:
  If invoked with no arguments, show current mbox statistics, otherwise
  switch current mbox (see mbox names sections).
* inc:
  Check for new messages in current mbox and load them if available.
* [page]z:
  Show the next page of mail listing.  A prefix could specify which page
  to show.
  * 2z: show the second page
  * -z: show previous page
  * +2z: show the 2nd next page
* m/mail [address]:
  Start a new mail; see writing mail section.
* [msgs]cp/copy [mbox]:
  Copy the specified messages to the given mbox.
* [msgs]mv/move [mbox]:
  Move the specified messages to the given mbox.
* [msg]r/reply:
  Reply the specified message.
* [msg]cat:
  Like page, pipes a message to PAGER except that all headers are present.
* [msgs]rm/d/delete:
  Remove specified messages.
* [msgs]u/undel/undelete:
  Clear specified messages if marked for deletion.
* q/quit:
  Save current mbox and exit.
* x/exit/xit:
  Exit mailx without saving current mbox.
* new/news:
  Show the list of mboxes that have new mails.
* [msgs]mime:
  Decode mime messages.

MAILBOXES
=========

Commands like cp, mv and cd take an mbox as argument.  The valid
values are:

* ',': The first mbox in "boxes" list that has new mails (see "news" command).
* '.': Current mbox.
* 'path/mbox': mbox with the given path.
* '+mbox': FOLDER/mbox

WRITING MAILS
=============

When mailx is invoked as "mailx address" or after mail or reply commands
mailx enters send mode.  In this mode the mail is written to and edited
in DRAFT file (that means no concurrent mail composition is allowed,
but that could be overcome easily if one needs to do so).  Commands are
specified after a tilde:

* ~e: edit the message in EDITOR
* ~v: edit the message in VISUAL
* ~x/~q: discard current message
* ~.: send current message (pipe into SENDMAIL) and append it to RECORD
* ~p: pipe current message into PAGER
