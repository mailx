#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include "util.h"

int file_size(int fd)
{
	struct stat stat;
	fstat(fd, &stat);
	return stat.st_size;
}

int xread(int fd, char *buf, int len)
{
	int nr = 0;
	while (nr < len) {
		int ret = read(fd, buf + nr, len - nr);
		if (ret == -1 && (errno == EAGAIN || errno == EINTR))
			continue;
		if (ret <= 0)
			break;
		nr += ret;
	}
	return nr;
}

int xwrite(int fd, char *buf, int len)
{
	int nw = 0;
	while (nw < len) {
		int ret = write(fd, buf + nw, len - nw);
		if (ret == -1 && (errno == EAGAIN || errno == EINTR))
			continue;
		if (ret < 0)
			break;
		nw += ret;
	}
	return nw;
}

void exec_file(char *filename, char **args)
{
	pid_t pid;
	if (!(pid = fork())) {
		execvp(filename, args);
		exit(1);
	}
	waitpid(pid, NULL, 0);
}

void exec_pipe(char *file, char **argv, char *s, int len)
{
	pid_t pid;
	int fds[2];
	pipe(fds);
	if (!(pid = fork())) {
		close(fds[1]);
		dup2(fds[0], STDIN_FILENO);
		execvp(file, argv);
		exit(1);
	}
	close(fds[0]);
	write(fds[1], s, len);
	close(fds[1]);
	waitpid(pid, NULL, 0);
}
