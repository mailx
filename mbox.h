#define MAXMAILS		(1 << 16)
#define MAXHDRS			(1 << 6)
#define MAXPATHLEN		(1 << 12)

#define STAT_NEW		0x01
#define STAT_OLD		0x02
#define STAT_READ		0x04
#define STAT_DEL		0x08

struct mail {
	char *hdrs[MAXHDRS + 1];
	int nhdrs;
	char *head;
	char *body;
	char *stat_hdr;
	int len;
	int body_len;
	unsigned orig_stat;
	unsigned stat;
};

struct mbox {
	struct mail mails[MAXMAILS];
	char path[MAXPATHLEN];
	int n;
	char *mbox;
	int len;
	int size;
};

struct mbox *mbox_alloc(char *filename);
void mbox_free(struct mbox *mbox);
int mbox_inc(struct mbox *mbox);
int mbox_write(struct mbox *mbox);

int mail_head(struct mail *mail, char *dst, int len, char **hdrs, int n);
char *mail_hdr(struct mail *mail, char *hdr);
char *mail_read(struct mail *mail, char *s, char *e);
void mail_write(struct mail *mail, int fd);
int hdr_len(char *hdr);
