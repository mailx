#include <ctype.h>
#include <string.h>
#include "mbox.h"
#include "mime.h"
#include "str.h"

static char *b64_ch =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static int b64_val[256];

static void b64_init(void)
{
	int i;
	for (i = 0; i < 64; i++)
		b64_val[(unsigned char) b64_ch[i]] = i;
}

static int b64_dec(char *d, char *s)
{
	unsigned v = 0;
	v |= b64_val[(unsigned char) s[0]];
	v <<= 6;
	v |= b64_val[(unsigned char) s[1]];
	v <<= 6;
	v |= b64_val[(unsigned char) s[2]];
	v <<= 6;
	v |= b64_val[(unsigned char) s[3]];

	d[2] = v & 0xff;
	v >>= 8;
	d[1] = v & 0xff;
	v >>= 8;
	d[0] = v & 0xff;
	return 3 - (s[1] == '=') - (s[2] == '=') - (s[3] == '=');
}

static char *dec_b64(char *dst, char *src, int len)
{
	char *end = src + len;
	if (!b64_val['B'])
		b64_init();
	while (src + 4 <= end) {
		while (src < end && isspace(*src))
			src++;
		if (src < end) {
			dst += b64_dec(dst, src);
			src += 4;
		}
	}
	return dst;
}

static int hexval(int c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	if (c >= 'A' && c <= 'F')
		return 10 + c - 'A';
	if (c >= 'a' && c <= 'f')
		return 10 + c - 'a';
	return 0;
}

static char *dec_qp(char *d, char *s, int len)
{
	static char t[1 << 10];
	char *end = s + len;
	int i;
	s--;
	while (++s < end) {
		if (*s != '=') {
			*d++ = *s;
			continue;
		}
		if (s[1] == '\n') {
			s++;
			continue;
		}
		if (s[1] == '?') {
			char *enc_qu = memchr(s + 2, '?', end - s - 4);
			if (enc_qu) {
				int b64 = tolower(enc_qu[1]) == 'b';
				char *b = enc_qu + 3;
				char *e = memchr(b, '?', end - b);
				if (e && sizeof(t) > e - b) {
					for (i = 0; i < e - b; i++)
						t[i] = b[i] == '_' ? ' ' : b[i];
					d = (b64 ? dec_b64 : dec_qp)(d, t, e - b);
					s = e + 1;
					continue;
				}
			}
		}
		*d++ = hexval(s[1]) << 4 | hexval(s[2]);
		s += 2;
	}
	return d;
}

#define MAXPARTS		(1 << 3)
#define BOUNDLEN		(1 << 7)

#define TYPE_TXT	0x00
#define TYPE_MPART	0x01
#define TYPE_ETC	0x02
#define ENC_B8		0x00
#define ENC_QP		0x10
#define ENC_B64		0x20

struct mime {
	int depth;
	int part[MAXPARTS];
	char bound[MAXPARTS][BOUNDLEN];
	char *src;
	char *dst;
	char *end;
};

static void copy_till(struct mime *m, char *s)
{
	int len = s - m->src;
	memcpy(m->dst, m->src, len);
	m->dst += len;
	m->src += len;
}

static void read_boundary(struct mime *m, char *s, char *hdrend)
{
	char *bound = m->bound[m->depth];
	char *e;
	s = memchr(s, '=', hdrend - s);
	if (!s)
		return;
	s++;
	if (*s == '"') {
		s++;
		e = memchr(s, '"', hdrend - s);
	} else {
		e = s;
		while (e < hdrend && !isspace(*e) && *e != ';')
			e++;
	}
	if (!e)
		return;
	bound[0] = '-';
	bound[1] = '-';
	memcpy(bound + 2, s, e - s);
	bound[e - s + 2] = '\0';
	m->depth++;
}

static char *hdr_nextfield(char *s, char *e)
{
	while (s && s < e && *s != ';')
		if (*s++ == '"')
			if ((s = memchr(s, '"', e - s)))
				s++;
	return s && s + 2 < e ? s + 1 : NULL;
}

static int read_hdrs(struct mime *m)
{
	char *s = m->src;
	char *e = m->end;
	int type = 0;
	while (s && s < e && *s != '\n') {
		char *n = memchr(s, '\n', e - s);
		while (n && n + 1 < e && n[1] != '\n' && isspace(n[1]))
			n = memchr(n + 1, '\n', e - n - 1);
		if (!n)
			break;
		n++;
		copy_till(m, s);
		if (startswith(s, "Subject:") || startswith(s, "From:") ||
				startswith(s, "To:") || startswith(s, "Cc:")) {
			m->dst = dec_qp(m->dst, m->src, n - s);
			m->src = n;
		}
		if (startswith(s, "Content-Type:")) {
			char *key = strchr(s, ':') + 1;
			char *hdrend = s + hdr_len(s);
			while (key) {
				while (key < hdrend && isspace(*key))
					key++;
				if (startswith(key, "text"))
					type |= TYPE_TXT;
				if (startswith(key, "multipart"))
					type |= TYPE_MPART;
				if (startswith(key, "boundary"))
					read_boundary(m, key, hdrend);
				key = hdr_nextfield(key, hdrend);
			}
		}
		if (startswith(s, "Content-Transfer-Encoding:")) {
			char *key = strchr(s, ':') + 1;
			char *hdrend = s + hdr_len(s);
			while (key) {
				while (key < hdrend && isspace(*key))
					key++;
				if (startswith(key, "quoted-printable"))
					type |= ENC_QP;
				if (startswith(key, "base64"))
					type |= ENC_B64;
				key = hdr_nextfield(key, hdrend);
			}
		}

		s = n;
	}
	copy_till(m, s ? s + 1 : e);
	return type;
}

static int is_bound(struct mime *m, char *s)
{
	return startswith(s, m->bound[m->depth - 1]);
}

static void read_bound(struct mime *m)
{
	char *s = m->src;
	int len = strlen(m->bound[m->depth - 1]);
	if (s[len] == '-' && s[len + 1] == '-')
		m->depth--;
	s = memchr(s, '\n', m->end - s);
	s = s ? s + 1 : m->end;
	copy_till(m, s);
}

static char *find_bound(struct mime *m)
{
	char *s = m->src;
	char *e = m->end;
	while (s < e) {
		if (is_bound(m, s))
			return s;
		if (!(s = memchr(s, '\n', e - s)))
			break;
		s++;
	}
	return e;
}

static void read_body(struct mime *m, int type)
{
	char *end = m->depth ? find_bound(m) : m->end;
	if (~type & TYPE_TXT) {
		copy_till(m, end);
		return;
	}
	if (type & ENC_QP) {
		m->dst = dec_qp(m->dst, m->src, end - m->src);
		m->src = end;
		return;
	}
	if (type & ENC_B64) {
		m->dst = dec_b64(m->dst, m->src, end - m->src);
		m->src = end;
		return;
	}
	copy_till(m, end);
}

int mime_decode(char *dst, char *src, int len)
{
	struct mime m;
	memset(&m, 0, sizeof(m));
	m.src = src;
	m.dst = dst;
	m.end = src + len;
	while ((m.depth && m.src < m.end) || m.src == src) {
		int type = read_hdrs(&m);
		read_body(&m, type);
		if (m.depth)
			read_bound(&m);
	}
	*m.dst = '\0';
	return m.dst - dst;
}
