CC = cc
CFLAGS = -Wall -O2
LDFLAGS =

all: mailx
%.o: %.c mbox.h config.h
	$(CC) -c $(CFLAGS) $<
mailx: mailx.o mbox.o sort.o util.o send.o str.o mime.o
	$(CC) -o $@ $^ $(LDFLAGS)
clean:
	rm -f *.o mailx
