#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <utime.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "mbox.h"
#include "str.h"
#include "util.h"

#define INCSIZE		(1 << 22)

static void set_atime(char *filename)
{
	struct stat st;
	struct utimbuf times;
	stat(filename, &st);
	times.actime = time(NULL);
	times.modtime = st.st_mtime;
	utime(filename, &times);
}

static char *mail_start(char *r, char *e)
{
	char *s = r;
	char *t;
	while (s < e) {
		if (!strncmp("From ", s, 5) && (s == r || *(s - 1) == '\n'))
			break;
		t = memchr(s + 1, 'F', e - s - 1);
		s = t ? t : e;
	}
	return s;
}

static char *mail_hdrs(struct mail *mail, char *s)
{
	static char stat[] = "Status:";
	char *r;
	while (s && *s && *s != '\r' && *s != '\n') {
		if (isalpha(*s) && mail->nhdrs < MAXHDRS)
			mail->hdrs[mail->nhdrs++] = s;
		if (!strncmp(stat, s, sizeof(stat) - 1))
			mail->stat_hdr = s;
		if ((r = strchr(s + 1, '\n')))
			s = r + 1;
		else
			s = strchr(s + 1, '\0');
	}
	mail->hdrs[mail->nhdrs] = s;
	return s;
}

char *mail_hdr(struct mail *mail, char *hdr)
{
	int i;
	for (i = 1; i < mail->nhdrs; i++)
		if (startswith(mail->hdrs[i], hdr))
			return mail->hdrs[i];
	return NULL;
}

int hdr_len(char *hdr)
{
	char *s = hdr;
	while (*s) {
		char *r = strchr(s, '\n');
		if (!r)
			return strchr(s, '\0') - hdr;
		s = r + 1;
		if (!isspace(*s))
			return s - hdr;
	}
	return 0;
}

static void mail_stat(struct mail *mail)
{
	if (mail->stat_hdr) {
		char *s = mail->stat_hdr + strlen("Status:");
		while (*s && *s != '\r' && *s != '\n') {
			switch (*s++) {
			case 'O':
				mail->stat |= STAT_OLD;
				break;
			case 'N':
				mail->stat |= STAT_NEW;
				break;
			case 'R':
				mail->stat |= STAT_READ;
				break;
			}
		}
		mail->orig_stat = mail->stat;
	} else {
		mail->stat = STAT_NEW;
	}
}

char *mail_read(struct mail *mail, char *s, char *e)
{
	char *end;
	mail->head = s;
	mail->body = mail_hdrs(mail, s);
	end = mail_start(mail->body, e);
	mail->len = end - mail->head;
	mail->body_len = end - mail->body;
	mail_stat(mail);
	return end;
}

static int read_mails(struct mbox *mbox, char *s, char *e)
{
	while (s && *s && mbox->n < MAXMAILS)
		s = mail_read(&mbox->mails[mbox->n++], s, e);
	return mbox->n == MAXMAILS;
}

int mbox_inc(struct mbox *mbox)
{
	int fd = open(mbox->path, O_RDONLY);
	int old_n = mbox->n;
	int old_len = mbox->len;
	int len = file_size(fd);
	if (fd == -1)
		return 0;
	if (!mbox->mbox) {
		mbox->size = len + INCSIZE + 1;
		mbox->mbox = malloc(mbox->size);
	}
	if (!mbox->mbox || len > mbox->size - 1)
		return -1;
	if (mbox->len)
		lseek(fd, mbox->len, SEEK_SET);
	mbox->len += xread(fd, mbox->mbox + mbox->len, len);
	mbox->mbox[mbox->len] = '\0';
	close(fd);
	set_atime(mbox->path);
	if (old_len < mbox->len)
		if (read_mails(mbox, mbox->mbox + old_len,
				mbox->mbox + mbox->len))
			return -1;
	return mbox->n - old_n;
}

struct mbox *mbox_alloc(char *filename)
{
	struct mbox *mbox;
	mbox = malloc(sizeof(*mbox));
	if (!mbox)
		return NULL;
	memset(mbox, 0, sizeof(*mbox));
	strcpy(mbox->path, filename);
	if (mbox_inc(mbox) < 0) {
		mbox_free(mbox);
		return NULL;
	}
	return mbox;
}

void mbox_free(struct mbox *mbox)
{
	free(mbox->mbox);
	free(mbox);
}

int mail_head(struct mail *mail, char *buf, int buf_len,
		char **hdrs, int n)
{
	int i, j;
	char *s = buf;
	char *dead = s + buf_len;
	for (i = 0; i < n; i++) {
		char *pat = hdrs[i];
		for (j = 0; j < mail->nhdrs; j++) {
			char *hdr = mail->hdrs[j];
			if (startswith(hdr, pat)) {
				int hdr_len = mail->hdrs[j + 1] - hdr;
				if (s + hdr_len > dead)
					hdr_len = dead - s;
				memcpy(s, hdr, hdr_len);
				s += hdr_len;
				break;
			}
		}
	}
	*s = '\0';
	return s - buf;
}

static int mail_changed(struct mail *mail)
{
	return mail->orig_stat != mail->stat;
}

void mail_write(struct mail *mail, int fd)
{
	char stat[32] = "Status: ";
	char *s = strchr(stat, '\0');
	if (mail->stat & STAT_READ)
		*s++ = 'R';
	if (mail->stat & (STAT_OLD | STAT_READ))
		*s++ = 'O';
	if (mail->stat & STAT_NEW)
		*s++ = 'N';
	*s++ = '\n';
	if (!mail_changed(mail)) {
		xwrite(fd, mail->head, mail->len);
	} else {
		char *hdr_beg = mail->body;
		char *hdr_end = mail->body;
		if (mail->stat_hdr) {
			char *nl = strchr(mail->stat_hdr, '\n');
			hdr_beg = mail->stat_hdr;
			hdr_end = nl ? nl + 1 : mail->body;
		}
		xwrite(fd, mail->head, hdr_beg - mail->head);
		xwrite(fd, stat, s - stat);
		if (hdr_end)
			xwrite(fd, hdr_end, mail->head + mail->len - hdr_end);
	}
}

static int mbox_changed(struct mbox *mbox)
{
	int i;
	for (i = 0; i < mbox->n; i++)
		if (mail_changed(&mbox->mails[i]))
			return 1;
	return 0;
}

int mbox_write(struct mbox *mbox)
{
	int fd;
	int i = 0;
	if (mbox_inc(mbox) < 0)
		return -1;
	if (!mbox_changed(mbox))
		return 0;
	fd = open(mbox->path, O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IWUSR);
	while (i < mbox->n) {
		char *beg = mbox->mails[i].head;
		char *end = mbox->mbox + mbox->len;
		while (i < mbox->n && !mail_changed(&mbox->mails[i]))
			i++;
		if (i < mbox->n)
			end = mbox->mails[i].head;
		xwrite(fd, beg, end - beg);
		if (i < mbox->n && !(mbox->mails[i].stat & STAT_DEL))
			mail_write(&mbox->mails[i], fd);
		i++;
	}
	close(fd);
	return 0;
}
