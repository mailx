/*
 * neatmailx, a small mailx clone
 *
 * Copyright (C) 2009-2014 Ali Gholami Rudi <ali at rudi dot ir>
 *
 * This program is released under the Modified BSD license.
 */
#include <ctype.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "config.h"
#include "mbox.h"
#include "mime.h"
#include "send.h"
#include "sort.h"
#include "str.h"
#include "util.h"

#define LEN(a)			(sizeof(a) / sizeof((a)[0]))
#define MAILBUF			(1 << 16)
#define MAXLINE			(1 << 8)
#define BUFSIZE			(1 << 12)

static struct mbox *mbox;		/* current mbox */
static struct mail *sorted[MAXMAILS];	/* sorted mails in mbox */
static int levels[MAXMAILS];		/* identation of mails in sorted[] */
static int mcount;			/* number of mails in sorted[] */
static int cur;				/* current mail */
static int sel[MAXMAILS];		/* list of selected mails */
static int nsel;			/* number of selected mails */
static int quit;

static int readcmd(char *dst, int size)
{
	return fgets(dst, size, stdin) == NULL;
}

static int utf8len(int c)
{
	if (c > 0 && c <= 0x7f)
		return 1;
	if (c >= 0xfc)
		return 6;
	if (c >= 0xf8)
		return 5;
	if (c >= 0xf0)
		return 4;
	if (c >= 0xe0)
		return 3;
	if (c >= 0xc0)
		return 2;
	return -1;
}

static char *till_eol(char *r, int len, char *s, char *e, int fill)
{
	int l, i = 0;
	while (i < len && s && s < e && *s) {
		l = utf8len((unsigned char) *s);
		if (*s == '\r' || *s == '\n') {	/* ignoring line breaks */
			s++;
		} else if (l <= 0) {
			*r++ = '?';
			s++;
			i++;
		} else {
			memcpy(r, s, l);
			r += l;
			s += l;
			i++;
		}
	}
	while (i++ < fill)
		*r++ = ' ';
	return r;
}

static int msg_num(char *num)
{
	int n = -1;
	if (!*num || !strcmp(".", num))
		n = cur;
	if (isdigit(*num))
		n = atoi(num);
	if (!strcmp("$", num))
		n = mbox->n - 1;
	if (*num == '-')
		n = cur - (*(num + 1) ? atoi(num + 1) : 1);
	if (*num == '+')
		n = cur + (*(num + 1) ? atoi(num + 1) : 1);
	if (!strcmp(",", num)) {
		int i = cur;
		while (++i < mcount) {
			int stat = sorted[i]->stat;
			if (!(stat & STAT_READ) && (stat & STAT_NEW)) {
				n = i;
				break;
			}
		}
	}
	if (n < 0 || n >= mbox->n)
		return -1;
	return n;
}

static int stat_char(struct mail *mail)
{
	if (mail->stat & STAT_NEW)
		return mail->stat & STAT_READ ? 'R' : 'N';
	else
		return mail->stat & STAT_READ ? ' ' : 'U';
}

/* search s for r */
static char *memstr(char *s, int slen, char *r, int rlen)
{
	char *d = s + slen;
	while (s && s < d) {
		if (!strncmp(s, r, MIN(d - s, rlen)))
			return s;
		s = memchr(s + 1, r[0], d - s - 1);
	}
	return NULL;
}

static int search(char *args, int all, int dir)
{
	static char hdr_name[MAXLINE];	/* previous search header */
	static char hdr_val[MAXLINE];	/* previous search keyword */
	static int stat;		/* previous search status */
	char *beg = strchr(args, '(');
	char *spc = beg ? strchr(beg, ' ') : NULL;
	char *end = spc ? strchr(spc, ')') : NULL;
	int i;
	if (beg && (!end || !spc))
		return 0;
	if (beg) {
		int hdr_len = spc - beg - 1;
		put_mem(hdr_name, beg + 1, hdr_len);
		hdr_name[hdr_len] = '\0';
		while (isspace(*spc))
			spc++;
		put_mem(hdr_val, spc, end - spc);
		hdr_val[end - spc] = '\0';
		stat = isalpha(*(args + 1)) ? toupper(*(args + 1)) : 0;
	}
	for (i = cur + dir; i >= 0 && i < mcount; i += dir) {
		if (!stat || stat_char(sorted[i]) == stat) {
			char *hdr = mail_hdr(sorted[i], hdr_name);
			if (hdr && memstr(hdr, hdr_len(hdr),
					hdr_val, strlen(hdr_val))) {
				if (!all) {
					cur = i;
					return 1;
				}
				sel[nsel++] = i;
			}
		}
	}
	return nsel;
}

static int sel_msgs(char *args, int all)
{
	char num[MAXLINE];
	int i;
	nsel = 0;
	if (*args == '/')
		return search(args, all, +1);
	if (*args == '?')
		return search(args, all, -1);
	args = cut_word(num, args);
	while (1) {
		char *com;
		int beg = -1;
		int end = -1;
		if (*num && (com = strchr(num + 1, ','))) {
			char beg_str[MAXLINE];
			memcpy(beg_str, num, com - num);
			beg_str[com - num] = '\0';
			beg = msg_num(beg_str);
			end = msg_num(com + 1);
		} else if (!strcmp("%", num)) {
			beg = 0;
			end = mcount - 1;
		} else {
			beg = msg_num(num);
			end = beg;
		}
		if (beg != -1 && end != -1) {
			if (!all) {
				cur = beg;
				return 1;
			}
			for (i = beg; i <= end; i++)
				sel[nsel++] = i;
		}
		args = cut_word(num, args);
		if (!*num)
			break;
	}
	return nsel;
}

static char mimes_buf[MAXMIME][MAILBUF];
static struct mail mimes_mail[MAXMIME];
static struct mail *mimes_main[MAXMIME];
static int mimes_cur;

static void mimes_clear(void)
{
	mimes_cur = 0;
	memset(mimes_main, 0, sizeof(mimes_main));
}

static void mimes_free(struct mail *mail)
{
	int i;
	for (i = 0; i < MAXMIME; i++)
		if (mimes_main[i] == mail)
			mimes_main[i] = NULL;
}

static void mimes_dec(struct mail *mail)
{
	struct mail *mime_mail = &mimes_mail[mimes_cur];
	int len;
	mimes_main[mimes_cur] = mail;
	len = mime_decode(mimes_buf[mimes_cur], mail->head,
				MIN(mail->len, MAILBUF));
	memset(mime_mail, 0, sizeof(*mime_mail));
	mail_read(mime_mail, mimes_buf[mimes_cur], mimes_buf[mimes_cur] + len);
	/* handle ^From_ inside msgs */
	mime_mail->body_len += len - mime_mail->len;
	mime_mail->len = len;
	mimes_cur = (mimes_cur + 1) % MAXMIME;
}

static struct mail *mimes_get(struct mail *mail)
{
	int i;
	for (i = 0; i < MAXMIME; i++)
		if (mimes_main[i] == mail)
			return &mimes_mail[i];
	return mail;
}

static void cmd_mime(char *pre, char *arg)
{
	int i;
	if (!sel_msgs(pre, 1))
		return;
	for (i = 0; i < nsel; i++)
		mimes_dec(sorted[sel[i]]);
}

static void show_mail(char *args, int filthdr)
{
	struct mail *mail;
	char buf[MAILBUF];
	char *pg_args[] = {PAGER, NULL};
	char *s = buf;
	if (!sel_msgs(args, 0))
		return;
	mail = sorted[cur];
	mail->stat |= STAT_READ;
	mail = mimes_get(mail);
	if (filthdr) {
		s += mail_head(mail, buf, sizeof(buf), hdr_filt, LEN(hdr_filt));
		s = put_mem(s, mail->body, MIN(sizeof(buf) - (s - buf),
					mail->body_len));
	} else {
		s = put_mem(s, mail->head, MIN(sizeof(buf), mail->len));
	}
	exec_pipe(PAGER, pg_args, buf, s - buf);
}

static void cmd_page(char *pre, char *arg)
{
	show_mail(pre, 1);
}

static void cmd_cat(char *pre, char *arg)
{
	show_mail(pre, 0);
}

static int isreply(char *s)
{
	return tolower(s[0]) == 'r' && tolower(s[1]) == 'e' && s[2] == ':';
}

static char *put_hdr(struct mail *mail, char *name, char *dst, int wid, int fill)
{
	char *hdr = mail_hdr(mail, name);
	if (hdr) {
		hdr = hdr + strlen(name) + 1;
		if (TRIM_RE && isreply(hdr))
			hdr += 3;
		while (*hdr == ' ')
			hdr++;
		dst = till_eol(dst, wid, hdr, hdr + hdr_len(hdr), fill ? wid : 0);
	} else {
		while (fill && --wid >= 0)
			*dst++ = ' ';
	}
	return dst;
}

#define put_clr(s, c)	((COLORS) ? (put_str(s, c)) : (s))

static void cmd_head(char *pre, char *arg)
{
	int beg, end;
	int i;
	if (!sel_msgs(pre, 0))
		return;
	beg = cur / NHEAD * NHEAD;
	end = MIN(beg + NHEAD, mbox->n);
	for (i = beg; i < end; i++) {
		struct mail *mail = sorted[i];
		char fmt[MAXLINE];
		char *s = fmt;
		int col;
		int indent;
		*s++ = i == cur ? '>' : ' ';
		s = put_clr(s, mail->stat & STAT_NEW ? "\33[1;31m" : "\33[33m");
		*s++ = mail->stat & STAT_DEL ? 'D' : ' ';
		*s++ = stat_char(mail);
		s = put_clr(s, "\33[1;32m");
		s = put_int(s, i, DIGITS);
		*s++ = ' ';
		*s++ = ' ';
		mail = mimes_get(mail);
		s = put_clr(s, "\33[0;34m");
		s = put_hdr(mail, "From:", s, 16, 1);
		*s++ = ' ';
		*s++ = ' ';
		col = 3 + DIGITS + 2 + 16 + 2;
		indent = levels[i] * 2;
		if (!mail_hdr(sorted[i], "in-reply-to"))
			s = put_clr(s, "\33[1;36m");
		else
			s = put_clr(s, indent ? "\33[32m" : "\33[36m");
		while (indent-- && col < WIDTH) {
			col++;
			*s++ = ' ';
		}
		s = put_hdr(mail, "Subject:", s, WIDTH - col, 0);
		s = put_clr(s, "\33[m");
		*s++ = '\n';
		write(1, fmt, s - fmt);
	}
}

static void cmd_z(char *pre, char *arg)
{
	int page = -1;
	if (!*pre)
		page = MIN(cur + NHEAD, mbox->n) / NHEAD;
	if (*pre == '-')
		page = cur / NHEAD - (*(pre + 1) ? atoi(pre + 1) : 1);
	if (*pre == '+')
		page = cur / NHEAD + (*(pre + 1) ? atoi(pre + 1) : 1);
	if (*pre == '$')
		page = mbox->n / NHEAD;
	if (isdigit(*pre))
		page = atoi(pre);
	if (page >= 0 && page * NHEAD < mbox->n) {
		cur = page * NHEAD;
		cmd_head("", "");
	}
}

static void cmd_del(char *pre, char *arg)
{
	int i;
	if (!sel_msgs(pre, 1))
		return;
	for (i = 0; i < nsel; i++)
		sorted[sel[i]]->stat |= STAT_DEL;
}

static void cmd_undel(char *pre, char *arg)
{
	int i;
	if (!sel_msgs(pre, 1))
		return;
	for (i = 0; i < nsel; i++) {
		sorted[sel[i]]->stat &= ~STAT_DEL;
		mimes_free(sorted[sel[i]]);
	}
}

static char *filename(char *path)
{
	char *slash = strrchr(path, '/');
	return slash ? slash + 1 : path;
}

static void mailx_sum(void)
{
	int new = 0, unread = 0, del = 0;
	int i;
	printf("%s: %8d msgs ", filename(mbox->path), mbox->n);
	for (i = 0; i < mbox->n; i++) {
		if (!(mbox->mails[i].stat & STAT_READ)) {
			unread++;
			if (mbox->mails[i].stat & STAT_NEW)
				new++;
		}
		if (mbox->mails[i].stat & STAT_DEL)
			del++;
	}
	if (new)
		printf(" %8d new", new);
	if (unread)
		printf(" %8d unread", unread);
	if (del)
		printf(" %8d del", del);
	printf("\n");
}

static int mailx_isok(char *filename)
{
	struct stat st;
	stat(filename, &st);
	return S_ISREG(st.st_mode);
}

static void mailx_sort(int from)
{
	int i;
	for (i = from; i < mbox->n; i++)
		sorted[i] = &mbox->mails[i];
	if (THREADED)
		sort_mails(sorted, levels, mbox->n);
}

static void mailx_open(char *filename)
{
	mbox = mbox_alloc(filename);
	if (!mbox) {
		printf("failed to open <%s>\n", filename);
		quit = 1;
		return;
	}
	mailx_sort(0);
	mcount = mbox->n;
	cur = 0;
	mailx_sum();
}

static void mailx_close(void)
{
	mimes_clear();
	mbox_free(mbox);
}

static void mailx_old(struct mbox *mbox)
{
	int i;
	for (i = 0; i < mbox->n; i++) {
		struct mail *mail = &mbox->mails[i];
		mail->stat = (mail->stat & ~STAT_NEW) | STAT_OLD;
	}
}

static int has_mail(char *path)
{
	struct stat st;
	if (stat(path, &st) == -1)
		return 0;
	return st.st_mtime > st.st_atime;
}

static void mailx_path(char *path, char *addr)
{
	int i;
	if (!strcmp(".", addr) && mbox) {
		strcpy(path, mbox->path);
		return;
	}
	if (!strcmp(",", addr)) {
		for (i = 0; i < LEN(boxes); i++) {
			if (has_mail(boxes[i])) {
				strcpy(path, boxes[i]);
				return;
			}
		}
	}
	if (*addr == '+')
		sprintf(path, "%s%s", FOLDER, addr + 1);
	else
		strcpy(path, addr);
}

static void warn_nomem(void)
{
	printf("no mem for new msgs\n");
}

static void cmd_fold(char *pre, char *arg)
{
	char path[1024];
	if (*arg) {
		mailx_path(path, arg);
		if (mailx_isok(path)) {
			mailx_old(mbox);
			if (mbox_write(mbox) == -1) {
				warn_nomem();
				return;
			}
			mailx_close();
			mailx_open(path);
		} else {
			printf("cannot open <%s>\n", path);
		}
	} else {
		mailx_sum();
	}
}

static void cmd_news(char *pre, char *arg)
{
	int i;
	for (i = 0; i < LEN(boxes); i++)
		if (has_mail(boxes[i]))
			printf("\t%s\n", filename(boxes[i]));
}

static void cmd_inc(char *pre, char *arg)
{
	int new = mbox_inc(mbox);
	if (new < 0)
		warn_nomem();
	mailx_sort(mcount);
	mcount = mbox->n;
	if (new > 0)
		printf("%d new\n", new);
}

static void cmd_next(char *pre, char *arg)
{
	if (!pre || !*pre) {
		if (cur + 1 < mbox->n) {
			cur++;
		} else {
			printf("EOF\n");
			return;
		}
	}
	show_mail(pre, 1);
}

static void copy_mail(char *msgs, char *dst, int del)
{
	char path[MAXLINE];
	int i;
	int fd;
	if (!dst || !*dst)
		return;
	if (!sel_msgs(msgs, 1))
		return;
	mailx_path(path, dst);
	fd = open(path, O_WRONLY | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR);
	if (fd == -1) {
		printf("failed to open <%s>\n", path);
		return;
	}
	for (i = 0; i < nsel; i++) {
		struct mail *mail = sorted[sel[i]];
		mail_write(mail, fd);
		if (del)
			mail->stat |= STAT_DEL;
	}
	close(fd);
}

static void cmd_copy(char *pre, char *arg)
{
	copy_mail(pre, arg, 0);
}

static void cmd_move(char *pre, char *arg)
{
	copy_mail(pre, arg, 1);
}

static void compose(struct draft *draft)
{
	char record[1024] = "";
	char line[MAXLINE];
	char cmd[MAXLINE];
	char *pg_args[] = {PAGER, NULL};
	if (RECORD)
		mailx_path(record, RECORD);
	else if (mbox)
		strcpy(record, mbox->path);
	while (!readcmd(line, sizeof(line))) {
		cut_word(cmd, line);
		if (!strcmp("~e", cmd))
			draft_edit(draft, EDITOR);
		if (!strcmp("~v", cmd))
			draft_edit(draft, VISUAL);
		if (!strcmp("~.", cmd)) {
			if (*record)
				draft_save(draft, record);
			draft_send(draft);
			break;
		}
		if (!strcmp("~p", cmd))
			exec_pipe(PAGER, pg_args, draft->mail, draft->len);
		if (!strcmp("~q", cmd) || !strcmp("~x", cmd))
			break;
	}
}

static void cmd_mail(char *pre, char *arg)
{
	struct draft draft;
	draft_init(&draft, *arg ? &arg : NULL, *arg ? 1 : 0, NULL);
	compose(&draft);
}

static void cmd_reply(char *pre, char *arg)
{
	struct draft draft;
	if (!sel_msgs(pre, 0))
		return;
	draft_reply(&draft, mimes_get(sorted[cur]));
	compose(&draft);
}

static void prompt(void)
{
	printf("? ");
	fflush(stdout);
}

static void cmd_quit(char *pre, char *arg)
{
	mailx_old(mbox);
	if (mbox_write(mbox) < 0)
		warn_nomem();
	else
		quit = 1;
}

static void cmd_exit(char *pre, char *arg)
{
	quit = 1;
}

static void cmd_clear(char *pre, char *arg)
{
	printf("\x1b[H\x1b[J");
	fflush(stdout);
}

static struct cmds {
	char *name;
	void (*cmd)(char *pre, char *arg);
} cmds[] = {
	{"page", cmd_page},
	{"next", cmd_next},
	{"header", cmd_head},
	{"ls", cmd_head},
	{"file", cmd_fold},
	{"folder", cmd_fold},
	{"inc", cmd_inc},
	{"z", cmd_z},
	{"mail", cmd_mail},
	{"copy", cmd_copy},
	{"cp", cmd_copy},
	{"move", cmd_move},
	{"mv", cmd_move},
	{"reply", cmd_reply},
	{"cat", cmd_cat},
	{"delete", cmd_del},
	{"rm", cmd_del},
	{"cd", cmd_fold},
	{"undelete", cmd_undel},
	{"quit", cmd_quit},
	{"exit", cmd_exit},
	{"xit", cmd_exit},
	{"news", cmd_news},
	{"mime", cmd_mime},
	{"clear", cmd_clear},
};

static void cmd_parse(char *line, char *pre, char *cmd, char *arg)
{
	while (*line && isspace(*line))
		line++;
	while (*line && !isalpha(*line)) {
		if ((line[0] == '/' || line[0] == '?') &&
				(line[1] == '(' || line[2] == '('))
			while (*line && *line != ')')
				*pre++ = *line++;
		*pre++ = *line++;
	}
	*pre = '\0';
	while (*line && isspace(*line))
		line++;
	while (*line && isalpha(*line))
		*cmd++ = *line++;
	*cmd = '\0';
	while (*line && isspace(*line))
		line++;
	while (*line && !isspace(*line))
		*arg++ = *line++;
	*arg = '\0';
}

static void loop(void)
{
	char line[MAXLINE];
	char pre[MAXLINE];
	char cmd[MAXLINE];
	char arg[MAXLINE];
	int len;
		int i;
	prompt();
	while (!quit && !readcmd(line, sizeof(line))) {
		cmd_parse(line, pre, cmd, arg);
		len = strlen(cmd);
		if (!len) {
			cmd_next(pre, arg);
		} else {
			for (i = 0; i < LEN(cmds); i++) {
				if (!strncmp(cmds[i].name, cmd, len)) {
					cmds[i].cmd(pre, arg);
					break;
				}
			}
		}
		if (!quit)
			prompt();
	}
}

int main(int argc, char *argv[])
{
	char *filename = NULL;
	char *subj = NULL;
	char path[1024];
	int i = 0;
	while (++i < argc) {
		if (argv[i][0] != '-')
			break;
		if (!strcmp("-f", argv[i]))
			filename = argv[++i];
		if (!strcmp("-s", argv[i]))
			subj = argv[++i];
	}
	signal(SIGPIPE, SIG_IGN);
	if (filename) {
		mailx_path(path, filename);
		if (mailx_isok(path)) {
			mailx_open(path);
			loop();
			mailx_close();
		} else {
			printf("cannot open <%s>\n", path);
		}
	} else {
		struct draft draft;
		draft_init(&draft, argv + i, argc - i, subj);
		compose(&draft);
	}
	return 0;
}
