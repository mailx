#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mbox.h"
#include "sort.h"

#define LEN(a)			(sizeof(a) / sizeof((a)[0]))

struct msg {
	struct mail *mail;
	char *id;	/* message-id header value */
	int id_len;	/* message-id length */
	char *rply;	/* reply-to header value */
	int rply_len;	/* reply-to length */
	int date;	/* message receiving date */
	int depth;	/* depth of message in the thread */
	int oidx;	/* the original index of the message */
	struct msg *parent;
	struct msg *head;
	struct msg *tail;
	struct msg *next;
};

static int id_cmp(char *i1, int l1, char *i2, int l2)
{
	if (l1 != l2)
		return l2 - l1;
	return strncmp(i1, i2, l1);
}

static int msgcmp_id(void *v1, void *v2)
{
	struct msg *t1 = *(struct msg **) v1;
	struct msg *t2 = *(struct msg **) v2;
	return id_cmp(t1->id, t1->id_len, t2->id, t2->id_len);
}

static int msgcmp_date(void *v1, void *v2)
{
	struct msg *t1 = *(struct msg **) v1;
	struct msg *t2 = *(struct msg **) v2;
	return t1->date == t2->date ? t1->oidx - t2->oidx : t1->date - t2->date;
}

static char *months[] = {
	"Jan", "Feb", "Mar", "Apr", "May", "Jun",
	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

static char *readtok(char *s, char *d)
{
	while (*s == ' ' || *s == ':')
		s++;
	while (!strchr(" \t\r\n:", *s))
		*d++ = *s++;
	*d = '\0';
	return s;
}

static int fromdate(char *s)
{
	char tok[128];
	int year, mon, day, hour, min, sec;
	int i;
	/* parsing "From ali Tue Apr 16 20:18:40 2013" */
	s = readtok(s, tok);		/* From */
	s = readtok(s, tok);		/* username */
	s = readtok(s, tok);		/* day of week */
	s = readtok(s, tok);		/* month name */
	mon = 0;
	for (i = 0; i < LEN(months); i++)
		if (!strcmp(months[i], tok))
			mon = i;
	s = readtok(s, tok);		/* day of month */
	day = atoi(tok);
	s = readtok(s, tok);		/* hour */
	hour = atoi(tok);
	s = readtok(s, tok);		/* minute */
	min = atoi(tok);
	s = readtok(s, tok);		/* seconds */
	sec = atoi(tok);
	s = readtok(s, tok);		/* year */
	year = atoi(tok);
	return ((year - 1970) * 400 + mon * 31 + day) * 24 * 3600 +
		hour * 3600 + min * 60 + sec;
}

static struct msg *msg_byid(struct msg **msgs, int n, char *id, int len)
{
	int l = 0;
	int h = n;
	while (l < h) {
		int m = (l + h) / 2;
		int d = id_cmp(id, len, msgs[m]->id, msgs[m]->id_len);
		if (!d)
			return msgs[m];
		if (d < 0)
			h = m;
		else
			l = m + 1;
	}
	return NULL;
}

static void msgs_tree(struct msg **all, struct msg **sorted_id, int n)
{
	int i;
	for (i = 0; i < n; i++) {
		struct msg *msg = all[i];
		struct msg *dad;
		if (!msg->rply)
			continue;
		dad = msg_byid(sorted_id, n, msg->rply, msg->rply_len);
		if (!dad)
			continue;
		msg->parent = dad;
		msg->depth = dad->depth + 1;
		if (!msg->parent->head)
			msg->parent->head = msg;
		else
			msg->parent->tail->next = msg;
		msg->parent->tail = msg;
	}
}

static void hdr_init(struct msg *msg)
{
	char *id_hdr = mail_hdr(msg->mail, "Message-ID:");
	char *rply_hdr = mail_hdr(msg->mail, "In-Reply-To:");
	if (id_hdr) {
		int len = hdr_len(id_hdr);
		char *beg = memchr(id_hdr, '<', len);
		char *end = beg ? memchr(id_hdr, '>', len) : NULL;
		if (beg && end) {
			while (*beg == '<')
				beg++;
			msg->id = beg;
			msg->id_len = end - beg;
		}
	}
	if (rply_hdr) {
		int len = hdr_len(rply_hdr);
		char *beg = memchr(rply_hdr, '<', len);
		char *end = beg ? memchr(rply_hdr, '>', len) : NULL;
		if (beg && end) {
			while (*beg == '<')
				beg++;
			msg->rply = beg;
			msg->rply_len = end - beg;
		}
	}
	msg->date = fromdate(msg->mail->head);
}

static struct msg **put_msg(struct msg **sorted, struct msg *msg)
{
	struct msg *cur = msg->head;
	*sorted++ = msg;
	while (cur) {
		sorted = put_msg(sorted, cur);
		cur = cur->next;
	}
	return sorted;
}

static void msgs_sort(struct msg **sorted, struct msg **msgs, int n)
{
	int i;
	for (i = 0; i < n; i++)
		if (!msgs[i]->parent)
			sorted = put_msg(sorted, msgs[i]);
}

int sort_mails(struct mail **mails, int *levels, int n)
{
	struct msg *msgs = malloc(n * sizeof(*msgs));
	struct msg **sorted_date = malloc(n * sizeof(*sorted_date));
	struct msg **sorted_id = malloc(n * sizeof(*sorted_id));
	struct msg **sorted = malloc(n * sizeof(*sorted));
	int i;
	if (!msgs || !sorted_date || !sorted_id) {
		free(msgs);
		free(sorted_date);
		free(sorted_id);
		free(sorted);
		return 1;
	}
	memset(msgs, 0, n * sizeof(*msgs));
	for (i = 0; i < n; i++) {
		struct msg *msg = &msgs[i];
		msg->oidx = i;
		msg->mail = mails[i];
		sorted_id[i] = msg;
		sorted_date[i] = msg;
		hdr_init(msg);
	}
	qsort(sorted_date, n, sizeof(*sorted_date), (void *) msgcmp_date);
	qsort(sorted_id, n, sizeof(*sorted_id), (void *) msgcmp_id);
	msgs_tree(sorted_date, sorted_id, n);
	msgs_sort(sorted, sorted_date, n);
	for (i = 0; i < n; i++)
		mails[i] = sorted[i]->mail;
	for (i = 0; i < n; i++)
		levels[i] = sorted[i]->depth;
	free(msgs);
	free(sorted_date);
	free(sorted_id);
	free(sorted);
	return 0;
}
