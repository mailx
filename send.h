#define MAXRECP			(1 << 5)
#define MAILBUF			(1 << 16)

struct draft {
	char mail[MAILBUF];
	int len;
};

void draft_init(struct draft *draft, char **to, int nto, char *subj);
void draft_send(struct draft *draft);
void draft_edit(struct draft *draft, char *editor);
void draft_save(struct draft *draft, char *path);
void draft_reply(struct draft *draft, struct mail *mail);
