#include <ctype.h>
#include <string.h>
#include "str.h"

char *put_mem(char *dst, char *src, int len)
{
	memcpy(dst, src, len);
	return dst + len;
}

char *put_str(char *dst, char *src)
{
	int len = strchr(src, '\0') - src;
	memcpy(dst, src, len + 1);
	return dst + len;
}

static char *put_int_fill(char *s, int n, int w, int f)
{
	int i;
	for (i = 0; i < w; i++) {
		s[w - i - 1] = n || !i ? '0' + n % 10 : f;
		n = n / 10;
	}
	return s + w;
}

char *put_int(char *s, int n, int w)
{
	return put_int_fill(s, n, w, ' ');
}

char *put_intz(char *s, int n, int w)
{
	return put_int_fill(s, n, w, '0');
}

char *cut_word(char *dst, char *s)
{
	while (*s && isspace(*s))
		s++;
	while (*s && !isspace(*s))
		*dst++ = *s++;
	*dst = '\0';
	return s;
}

/* return 1 if r starts with s */
int startswith(char *r, char *s)
{
	while (*s)
		if (tolower(*s++) != tolower(*r++))
			return 0;
	return 1;
}
