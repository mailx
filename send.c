#include <ctype.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "config.h"
#include "mbox.h"
#include "util.h"
#include "send.h"
#include "str.h"

#define USERAGENT		"git://repo.or.cz/mailx.git"
#define MAXLINE			(1 << 7)

static int isaddr(int c)
{
	return isalpha(c) || isdigit(c) || strchr("_.-@", c);
}

static char *cut_addr(char *dst, char *src)
{
	while (*src && !isaddr(*src))
		src++;
	while (isaddr(*src))
		*dst++ = *src++;
	*dst++ = '\0';
	return src;
}

static int parse_addr(char **recips, int size, char *s, int len)
{
	int n = 0;
	char *d = s + len;
	while (n < size && s < d && *(s = cut_addr(recips[n], s)))
		if (strchr(recips[n], '@'))
			n++;
	return n;
}

static int mail_recips(char **recips, int size, char *s, char *e)
{
	struct mail mail = {{0}};
	char *to, *cc;
	int n = 0;
	mail_read(&mail, s, e);
	if ((to = mail_hdr(&mail, "To:")))
		n += parse_addr(recips + n, size - n, to + 3, hdr_len(to) - 3);
	if ((cc = mail_hdr(&mail, "CC:")))
		n += parse_addr(recips + n, size - n, cc + 3, hdr_len(cc) - 3);
	return n;
}

static char *put_from_(char *s)
{
	time_t t;
	char *logname = getenv("LOGNAME");
	time(&t);
	s = put_str(s, "From ");
	s = put_str(s, logname ? logname : "me");
	s += strftime(s, MAXLINE, " %a %b %d %H:%M:%S %Y\n", localtime(&t));
	return s;
}

static char *put_date(char *s)
{
	time_t t;
	time(&t);
	s = put_str(s, "Date: ");
	s += strftime(s, MAXLINE, "%a, %d %b %Y %H:%M:%S %z\n", localtime(&t));
	return s;
}

static char *put_id(char *s)
{
	time_t t;
	time(&t);
	s = put_str(s, "Message-ID: <");
	s += strftime(s, MAXLINE, "%Y%d%m%H%M%S", localtime(&t));
	s = put_str(s, "@" HOSTNAME ">\n");
	return s;
}

static char *put_from(char *s)
{
	return FROM ? put_str(s, "From: " FROM "\n") : s;
}

static char *put_to(char *s, char **to, int n)
{
	int i;
	s = put_str(s, "To: ");
	s = put_str(s, to[0]);
	for (i = 1; i < n; i++) {
		s = put_str(s, ",\n\t");
		s = put_str(s, to[i]);
	}
	if (*(s - 1) != '\n')
		s = put_str(s, "\n");
	return s;
}

static char *put_subj(char *s, char *subj)
{
	s = put_str(s, "Subject: ");
	s = put_str(s, subj);
	s = put_str(s, "\n");
	return s;
}

static char *put_agent(char *s)
{
	return put_str(s, "User-Agent: " USERAGENT "\n");
}

void draft_init(struct draft *draft, char **to, int nto, char *subj)
{
	char *s = draft->mail;
	s = put_from_(s);
	s = put_date(s);
	s = put_from(s);
	if (to && nto)
		s = put_to(s, to, nto);
	if (subj)
		s = put_subj(s, subj);
	s = put_id(s);
	s = put_agent(s);
	draft->len = s - draft->mail;
}

static char *hdr_val(char *hdr)
{
	hdr = strchr(hdr, ':') + 1;
	while (isspace(*hdr))
		hdr++;
	return hdr;
}

static char *put_hdr(char *s, char *hdr)
{
	char *r = hdr_val(hdr);
	return put_mem(s, r, hdr_len(r));
}

static char *put_hdr_append(char *s, char *hdr)
{
	char *r = hdr_val(hdr);
	while (strchr(" \r\n", s[-1]))
		s--;
	s = put_str(s, s[-1] == ':' ? " " : ",\n\t");
	return put_mem(s, r, hdr_len(r));
}

static char *put_replysubj(char *s, char *subj)
{
	subj = hdr_val(subj);
	s = put_str(s, "Subject: ");
	if (tolower(subj[0]) != 'r' || tolower(subj[1]) != 'e')
		s = put_str(s, "Re: ");
	s = put_mem(s, subj, hdr_len(subj));
	return s;
}

static char *put_replyto(char *s, char *id, char *ref)
{
	s = put_str(s, "In-Reply-To: ");
	id = hdr_val(id);
	s = put_mem(s, id, hdr_len(id));
	s = put_str(s, "References: ");
	if (ref) {
		ref = hdr_val(ref);
		s = put_mem(s, ref, hdr_len(ref));
		s = put_str(s, "\t");
	}
	s = put_mem(s, id, hdr_len(id));
	return s;
}

static char *put_reply(char *s, char *from, char *to, char *cc, char *rply)
{
	if (from || rply) {
		s = put_str(s, "To: ");
		s = put_hdr(s, rply ? rply : from);
	}
	if (to || cc || (rply && from)) {
		s = put_str(s, "Cc: ");
		if (to)
			s = put_hdr_append(s, to);
		if (rply && from)
			s = put_hdr_append(s, from);
		if (cc)
			s = put_hdr_append(s, cc);
	}
	return s;
}

static char *quote_body(char *s, int size, struct mail *mail)
{
	char *from = mail_hdr(mail, "From:");
	char *r = mail->body;
	char *rd = r + mail->body_len;
	char *sd = s + size - 16;
	s = put_str(s, "\n");
	if (from) {
		from = hdr_val(from);
		s = put_mem(s, from, hdr_len(from) - 1);
		s = put_str(s, " wrote:\n");
	}
	while (s < sd && r < rd) {
		char *nl = memchr(r, '\n', rd - r);
		s = put_str(s, "> ");
		nl = nl ? nl + 1 : rd;
		s = put_mem(s, r, MIN(sd - s, nl - r));
		r = nl;
	}
	*s = '\0';
	return s;
}

void draft_reply(struct draft *draft, struct mail *mail)
{
	char *s = draft->mail;
	char *id_hdr = mail_hdr(mail, "Message-ID:");
	char *ref_hdr = mail_hdr(mail, "References:");
	char *from_hdr = mail_hdr(mail, "From:");
	char *subj_hdr = mail_hdr(mail, "Subject:");
	char *to_hdr = mail_hdr(mail, "To:");
	char *cc_hdr = mail_hdr(mail, "CC:");
	char *rply_hdr = mail_hdr(mail, "Reply-To:");

	s = put_from_(s);
	s = put_date(s);
	s = put_from(s);
	s = put_reply(s, from_hdr, to_hdr, cc_hdr, rply_hdr);
	if (subj_hdr)
		s = put_replysubj(s, subj_hdr);
	s = put_id(s);
	if (id_hdr)
		s = put_replyto(s, id_hdr, ref_hdr);
	s = put_agent(s);
	s = quote_body(s, sizeof(draft->mail) - (s - draft->mail), mail);
	draft->len = s - draft->mail;
}

void draft_edit(struct draft *draft, char *editor)
{
	char *edit_argv[] = {NULL, DRAFT, NULL};
	int fd;
	if ((fd = open(DRAFT, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR)) == -1)
		return;
	xwrite(fd, draft->mail, draft->len);
	close(fd);
	edit_argv[0] = editor;
	exec_file(editor, edit_argv);
	if ((fd = open(DRAFT, O_RDONLY)) == -1)
		return;
	draft->len = xread(fd, draft->mail, sizeof(draft->mail) - 1);
	close(fd);
	draft->mail[draft->len] = '\0';
	unlink(DRAFT);
}

void draft_send(struct draft *draft)
{
	char recips[MAXRECP][MAXLINE];
	char *send_argv[MAXRECP + 3] = {SENDMAIL, "-i"};
	char *beg = strchr(draft->mail, '\n') + 1;
	char *end = draft->mail + draft->len;
	int nrecips;
	int i;
	for (i = 0; i < MAXRECP; i++)
		send_argv[i + 2] = recips[i];
	nrecips = mail_recips(send_argv + 2, MAXRECP,
				draft->mail, draft->mail + draft->len);
	send_argv[2 + nrecips] = NULL;
	exec_pipe(SENDMAIL, send_argv, beg, end - beg);
}

static void write_nofrom(int fd, char *s, int len)
{
	char *beg = s;
	char *d = s + len;
	char *r;
	while (s < d) {
		if (!strncmp("\nFrom ", s, 6)) {
			write(fd, beg, s - beg + 1);
			write(fd, ">", 1);
			beg = s + 1;
		}
		r = strchr(s + 1, '\n');
		s = r ? r : d;
	}
	write(fd, beg, s - beg);
}

void draft_save(struct draft *draft, char *path)
{
	int fd = open(path, O_WRONLY | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR);
	if (fd < -1)
		return;
	write_nofrom(fd, draft->mail, draft->len);
	write(fd, "\n", 1);
	close(fd);
}
