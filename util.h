#define MIN(a, b)		((a) < (b) ? (a) : (b))

int file_size(int fd);
int xread(int fd, char *buf, int len);
int xwrite(int fd, char *buf, int len);
void exec_file(char *filename, char **args);
void exec_pipe(char *file, char **argv, char *s, int len);
