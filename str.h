char *put_int(char *s, int n, int w);
char *put_intz(char *s, int n, int w);
char *put_str(char *dst, char *src);
char *put_mem(char *dst, char *src, int len);
char *cut_word(char *dst, char *s);
int startswith(char *r, char *s);
